FROM nginxinc/nginx-unprivileged:1-alpine
LABEL maintainer="redspoutnik"

COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params

ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

USER root
RUN chmod 775 /etc/nginx/default.conf.tpl

RUN mkdir -p /vol/static
RUN chmod 755 /vol/static
RUN touch /etc/nginx/conf.d/default.conf
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf
RUN chmod 775 /etc/nginx/conf.d/default.conf
RUN ls -lisa /etc/nginx/conf.d/default.conf
RUN ls -lisa /etc/nginx/default.conf.tpl
RUN chown -R nginx:nginx /etc/nginx/ 
RUN ls -lisa /etc/nginx/uwsgi_params

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

USER nginx
#RUN ls -lisa /entrypoint.sh && /entrypoint.sh

CMD ["/entrypoint.sh"]
